import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import App from './App';
import { threeVerticesAndTwoEdges } from './testing/data'

const rawNetworkData = JSON.stringify(threeVerticesAndTwoEdges)

test('updates network graph when network input changes', () => {
  const { getByText, getByRole, container } = render(<App />);
  
  // initially no nodes/edges are rendered
  expect(container.querySelectorAll('circle').length).toEqual(0)
  expect(container.querySelectorAll('line').length).toEqual(0)

  // Enter network data
  fireEvent.change(getByRole("textbox"), {
    target: {
    value: `${rawNetworkData}`
  }});
  fireEvent.click(getByText("Enter"))

  // nodes/edges are updated
  expect(container.querySelectorAll('circle').length).toEqual(3)
  expect(container.querySelectorAll('line').length).toEqual(2)
  
});
